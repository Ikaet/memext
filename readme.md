# Memories Ekarus Mind Extension (MEMExt)

## Descripción del Proyecto

Memory Ekarus Mind Extension (MEMExt) es una aplicación web desarrollada con Vite, TypeScript y React. La aplicación tiene como objetivo proporcionar un espacio para almacenar pensamientos, notas de comandos y códigos relacionados con el desarrollo de software. Utiliza una base de datos MongoDB montada en una imagen de Docker para gestionar y almacenar la información de manera eficiente.

## Características Principales

- Almacenamiento Seguro: Los pensamientos, notas y códigos se almacenan de forma segura en una base de datos MongoDB.
- Interfaz de Usuario Intuitiva: La interfaz de usuario está diseñada de manera intuitiva para una experiencia de usuario fluida.
- Gestión Eficiente: Facilita la gestión y organización de información relacionada con el desarrollo de software.

## Tecnologías Utilizadas

- Vite: Herramienta de desarrollo rápida para proyectos basados en JavaScript y TypeScript.
- React: Biblioteca de JavaScript para construir interfaces de usuario.
- TypeScript: Superset de JavaScript que agrega tipado estático.
- MongoDB: Sistema de gestión de bases de datos NoSQL.
- Docker: Plataforma para el desarrollo, envío y ejecución de aplicaciones en contenedores.

## Requisitos Previos

Asegúrate de tener instalado Node.js y Docker en tu sistema antes de ejecutar la aplicación.

- Node.js
- Docker

## Configuración del Proyecto

1. Clona este repositorio en tu máquina local.

```bash
    git clone git@gitlab.com:Ikaet/memext.git
```

2. Accede al directorio del proyecto.

```bash
    cd memext
```

3. Instala las dependencias.

```bash
    npm install
```

## Configuración de la Base de Datos

1. Ejecuta MongoDB en un contenedor Docker.

```bash
    docker run -d -p 27017:27017 --name memext-mongodb mongo
```

2. Configura la conexión a la base de datos en el archivo .env.

```bash
    REACT_APP_MONGO_DB_URI=mongodb://localhost:27017/memext
```

## Ejecución de la Aplicación

1. Inicia la aplicación.

```bash
    npm run dev
```

1. Abre tu navegador y accede al servicio para utilizar MEMExt.

## Contribución

Si deseas contribuir al desarrollo de MEMExt, sigue estos pasos:

1. Realiza un fork del repositorio.
2. Crea una rama para tu contribución.

```bash
    git checkout -b feature/nueva-funcionalidad
```

3. Realiza tus cambios y commits.

```bash
    git add .
    git commit -m "Añade nueva funcionalidad"
```

4. Haz push a tu rama.

```bash
    git push origin feature/nueva-funcionalidad
```

5. Crea un pull request.

## Licencia

Este proyecto está bajo la licencia MIT.
